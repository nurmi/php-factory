FROM debian:buster-slim
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y --no-install-recommends \
    apt-utils \
    locales \
    autoconf \
    ca-certificates \
    sqlite3 \
    libxml2-dev \
    libsqlite3-dev \
    libcurl4-openssl-dev \
    libjpeg-dev \
    libpng-dev \
    libxpm-dev \
    libmariadbclient-dev \
    libpq-dev \
    libsodium-dev \
    argon2 \
    libargon2-0 \ 
    libargon2-1 \
    libargon2-dev \
    libicu-dev \
    libfreetype6-dev \
    libldap2-dev \
    libxslt-dev \
    libssl-dev \
    libldb-dev \
    libonig-dev \
    build-essential && \
    rm -rf /var/lib/apt/lists/* && \
    localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8
WORKDIR /php/src
