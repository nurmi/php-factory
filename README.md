# Build image
```
$ git clone git@bitbucket.org:nurmi/php-factory.git
$ cd php-factory
$ docker build -t php-factory .
```

# Build PHP
```
$ cd <some path>
$ mkdir build
$ wget https://www.php.net/distributions/php-7.4.0.tar.gz
$ tar xvf php-7.4.0.tar.gz
$ ln -s php-7.4.0 src
$ docker run --rm -v `pwd`:/php php-factory /php/src/configure --prefix=/php/build --with-openssl --with-pdo-mysql --enable-mbstring --with-password-argon2 --with-sodium 
$ docker run --rm -v `pwd`:/php php-factory make
$ docker run --rm -v `pwd`:/php php-factory make install
```

PHP is now installed under 'build' directory.

https://www.php.net/manual/en/mbstring.installation.php
--enable-mbstring requires libonig package or use --disable-mbregex configure option

Use ```docker run --rm -v `pwd`:/php php-factory /php/src/configure --help``` to get full list of options.

Also sometimes needed:
```
--enable-sockets
--enable-bcmath
```

# Optional steps 
Create personal bin directory (eg $HOME/bin) and place script called "php" in that folder:
```
$ cat $HOME/bin/php
#!/bin/sh
VERSION=73
$HOME/Apps/php/${VERSION}/build/bin/php -d include_path=$HOME/Apps/php/${VERSION}/build/lib/php $@
$ chmod 755 $HOME/bin/php
```
In this case my above "<some path>" was $HOME/Apps/php/73

